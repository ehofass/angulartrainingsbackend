var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ProductSchema = new Schema({
  image: {type: String, required: false, max: 100},
  carMake: {type: String, required: true, max: 100},
  carYear: {type: String, required: true, max: 100},
  carPrice: {type: Number, required: true},
  estimateRepair: {type: Number, required: true},
  repairPrice: {type: Number, required: false},
  created_time: Date,
  owner_id: {type: String, required: false},
  ip_address: {type: String, required: false, max: 100}
});


// Export the model
module.exports = mongoose.model('Product', ProductSchema);
