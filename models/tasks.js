var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var TasksSchema = new Schema({
  columnId: {type: String, required: true, max: 100},
  title: {type: String, required: false, max: 28},
  description: {type: String, required: false, max: 255},
  labels: {type: Array, required: false},
  assignee: {type: String, required: false}
});


// Export the model
module.exports = mongoose.model('Tasks', TasksSchema);
