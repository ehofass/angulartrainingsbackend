var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CoffeBreak = new Schema({
    ip_address: {type: String, required: true, max: 100}
});


// Export the model
module.exports = mongoose.model('CoffeBreak', CoffeBreak);