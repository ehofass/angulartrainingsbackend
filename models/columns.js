var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ColumnsSchema = new Schema({
  title: {type: String, required: false, max: 100},
  color: {type: String, required: false, max: 20},
});


// Export the model
module.exports = mongoose.model('Columns', ColumnsSchema);
