var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Users = new Schema({
    ip_address: {type: String, required: true, max: 100},
    name: {type: String, required: false, max: 100}
});


// Export the model
module.exports = mongoose.model('Users', Users);
