var express = require('express');
var router = express.Router();

var alexra_controller = require('../controllers/axelra');


// Routes for axelra FE challenge
router.post('/hello', alexra_controller.hello);

router.post('/clear_all_data', alexra_controller.clear_room);

router.post('/users', alexra_controller.all_users);

router.get('/tasks', alexra_controller.all_tasks);

router.post('/tasks', alexra_controller.create_task);

router.put('/tasks/:id', alexra_controller.update_task);

router.delete('/tasks/:id', alexra_controller.delete_task);



router.get('/columns', alexra_controller.all_columns);

router.post('/columns', alexra_controller.create_column);

router.put('/columns/:id', alexra_controller.update_column);

router.delete('/columns/:id', alexra_controller.delete_column);


module.exports = router;
