var express = require('express');
var router = express.Router();

var product_controller = require('../controllers/product');


// Routes for axelra FE challenge
router.get('/hello', product_controller.test);



router.get('/test', product_controller.test);

router.post('/buy_car/:id', product_controller.buy_car);

router.get('/dealership_cars', product_controller.product_list);
router.get('/garage/:owner_id', product_controller.product_list_by_ip);
router.post('/create_car', product_controller.product_create);
router.post('/sell_car/:id', product_controller.product_delete_by_owner);



router.post('/products', product_controller.product_create);

router.get('/products', product_controller.product_list);

router.get('/products/:id', product_controller.product_details);

router.put('/products/:id', product_controller.product_update);

router.delete('/products/:id', product_controller.product_delete);


router.delete('/delete-all-vat-va-taip-vat-tiesiog', product_controller.delete_everything);


router.post('/coffee', product_controller.request_coffe_break);
router.get('/get-coffee-count', product_controller.get_coffe_break_count);


module.exports = router;
