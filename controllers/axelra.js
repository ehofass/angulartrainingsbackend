var mongoose = require('./../node_modules/mongoose');


var Tasks = require('../models/tasks');
var Users = require('../models/users');
var Columns = require('../models/columns');

var animals = require('../lists/animals'); // LENGHT = 224
var adjectives = require('../lists/adjectives'); // LENGTH = 228

let allUsers = [];

const websocketMessages = {
  create: 'created new',
  update: 'updated a',
  delete: 'deleted a',
};

const SESSION_PING_INTERVAL = 15000;


exports.set_wss = (ws) => {
  wss = ws;
  // console.log('wss', wss);

  const interval = setInterval(function ping() {
    if (wss.clients.length === 0) {
      clearInterval(interval);
    }

    wss.clients.forEach(function each(ws) {
      if (ws.isAlive === false) {
        return ws.terminate();
      }

      ws.isAlive = false;
      ws.send('ping');
    });
  }, SESSION_PING_INTERVAL);

  wss.on('connection', function connection(ws, req) {
    ws.isAlive = true;
    ws.on('message', () => ws.isAlive = true);
    ws.remoteAddress = req.socket.remoteAddress;

    ws.on('close', (event) => {
      disconnectUser(req.socket.remoteAddress);
    });
  });
};

function disconnectUser(userId) {
  Users.findOne({ip_address: userId}).then(user => {
    Users.findOneAndDelete({ip_address: userId}).then(() => {
      if (wss) wss.clients.forEach((c) => {
        const message = `${user.name} has left :(`;
        c.send(JSON.stringify({type: 'session', message}));
      })
    });
  });
}

function sentWebsocketMessage(type, creatorIp, action, object) {
  const user = allUsers.find(u => u.ip_address === creatorIp);

  if (!user || !user.name) return;

  if (wss) wss.clients.forEach(function each(client) {
    const message = {
      type,
      message: `${user && user.name} ${websocketMessages[action]} ${type}`,
      object: object,
      action: action,
      creator_id: creatorIp
    };
    client.send(JSON.stringify(message));
  });
}

function generateRandomName() {
  const adjectiveNo = (Math.random() * 228) | 0;
  const nameNo = (Math.random() * 224) | 0;

  return `${adjectives[adjectiveNo]} ${animals[nameNo]}`;
}


exports.hello = function (req, res, next) {
  Users.find({ip_address: req.body.id} || '').then(foundUsers => {
    if (foundUsers.length === 0) {
      const newUserName = generateRandomName();
      var newUser = new Users({ip_address: req.connection.remoteAddress, name: newUserName});

      newUser.save(function (err, response) {

        if (wss) wss.clients.forEach((c) => {
          const message = `${newUser.name} has joined! :)`;
          c.send(JSON.stringify({type: 'session', message}));
        })

        res.send({
          success: true,
          data: {
            user_id: req.connection.remoteAddress,
            status: {success: true, message: 'OK', timestamp: new Date()}
          }
        });
        if (err) {
          return next(err);
        }
      });
    } else {
      res.send({
        success: true,
        data: {
          user_id: foundUsers[0].ip_address,
          status: {success: true, message: 'OK', timestamp: new Date()}
        }
      });
    }
  });
};

exports.all_users = function (req, res, next) {
  Users.find({}, {}, function (err, data) {
    if (err) {
      response = {"error": true, "message": "Error fetching users"};
    } else {
      response = data.map(u => u.ip_address === req.body.id ? {...u._doc, you: true} : u);
      allUsers = response;

      res.send({success: true, data: response});
    }
  });
}

exports.all_columns = function (req, res, next) {
  Columns.find({}, {}, function (err, data) {
    // Mongo command to fetch all data from collection.
    if (err) {
      response = {"error": true, "message": "Error fetching columns"};
    } else {
      response = data;
      const returnList = [
        {id: '1', title: 'TODO', color: 'rgba(26, 40, 73, .3)', textColor: '#FFF'},
        {id: '2', title: "In progress", color: 'rgba(176, 99, 197, .3)', textColor: '#FFF'}];
      res.send({success: true, data: response});
    }
  });
};

exports.create_column = function (req, res, next) {
  let column = new Columns(
    {
      title: req.body.title,
      color: req.body.color
    }
  );

  column.save(function (err) {
    if (err) {
      console.log('error:', err);
      return next(err);
    }
    console.log('column:', column);
    res.send({success: true, data: column});

    sentWebsocketMessage('column', req.body.id, 'create', column);
  });
};


exports.update_column = function (req, res, next) {
  Columns.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, column) {
    if (err) {
      console.log('error:', err);
      return next(err);
    }

    res.send({success: true, data: {...req.body, loading: false}});

    sentWebsocketMessage('column', req.body.id, 'update', req.body);

  });
};

exports.delete_column = function (req, res, next) {
  Columns.findById(req.params.id, function (err, column) {
    if (!column) {
      res.status(404).send({message: "No column with this id found"});
    } else {
      Columns.findByIdAndRemove(req.params.id, function (err) {
        if (err) return send(err);
        res.send({success: true, data: {_id: req.params.id}});

        sentWebsocketMessage('column', req.body.id, 'delete', column);
        deleteTasksForColumnId(req.params.id);
      })
    }
  });
};


function deleteTasksForColumnId(columnId) {
  var query = {columnId};
  Tasks.deleteMany(query, function (err) {
    console.log('deleted tasks for column', columnId);
  })
}


// TASKS

exports.all_tasks = function (req, res, next) {
  Tasks.find({}, {}, function (err, data) {
    // Mongo command to fetch all data from collection.
    if (err) {
      // response = {"error": true, "message": "Error fetching tasks"};
    } else {
      const response = data;
      res.send({success: true, data: response});
    }
  });
};

exports.create_task = function (req, res, next) {
  let task = new Tasks(
    {
      columnId: req.body.columnId,
      title: req.body.title || '',
      assigne: req.body.assigne || '',
      labels: req.body.labels || [],
    }
  );

  task.save(function (err) {
    if (err) {
      console.log('error:', err);
      return next(err);
    }
    console.log('task:', task);
    res.send({success: true, data: task});
    sentWebsocketMessage('task', req.body.id, 'create', task);

  })
};

exports.update_task = function (req, res, next) {
  Tasks.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, task) {
    if (err) {
      console.log('error:', err);
      return next(err);
    }

    const newTask = {...req.body, editing: false, loading: false};
    res.send({success: true, data: newTask});
    sentWebsocketMessage('task', req.body.id, 'update', newTask);

  });
};

exports.delete_task = function (req, res, next) {
  Tasks.findById(req.params.id, function (err, task) {
    if (!task) {
      res.status(404).send({message: "No task with this id found"});
    } else {
      Tasks.findByIdAndRemove(req.params.id, function (err) {
        if (err) return send(err);
        res.send({success: true, data: {_id: req.params.id}});
        sentWebsocketMessage('task', req.body.id, 'delete', task);

      })
    }
  });
};

exports.clear_room = function (req, res, next) {
  Tasks.deleteMany({}, function (err) {
  });

  Columns.deleteMany({}, function (err) {
  });

  Users.deleteMany({}, function (err) {
  });

  res.send({success: true});
};
