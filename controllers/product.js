var mongoose = require('./../node_modules/mongoose');

var Product = require('../models/product');
var CoffeBreak = require('../models/coffeBreak');
var player = require('play-sound')(opts = {});
var opn = require('opn');


var play = require('play');


var timesRequested = 0;
var counter = 0;
var requestsTillCoffeeBreak = 8;

var myIpAddress = '192.168.1.184:3001';

exports.test = function (req, res) {
  res.send(`
<div style="padding: 100px; font-size: 50px; display: flex;
    justify-content: center">Welcome to Angular Trainings!</div>
<div style="padding: 12px; font-size: 30px; border-bottom: 1px solid #eaeaea"> List of endpoints:</div>
<div style="padding: 30px;">
    <div style="font-size: 25px; display: flex; flex-direction: row"> <div style="width: 100px; color: orange; border-radius: 12px">POST</div><div style="border-left: 1px solid black; padding-left: 12px;"></div>    <div style="width:250px">Request Coffee Break:</div>  <a style="font-size: 20px; align-self: center; padding-left: 12px;">${myIpAddress}/trainings/coffee</a></div>
    <br>
    <div style="font-size: 25px; display: flex; flex-direction: row"> <div style="width: 100px; color: green; border-radius: 12px">GET</div><div style="border-left: 1px solid black; padding-left: 12px;"></div>      <div style="width:250px">List All Products:</div>    <a style="font-size: 20px; align-self: center; padding-left: 12px;">${myIpAddress}/trainings/products  &nbsp;&nbsp; pagination params: page (starts from 1), size</a></div>
    <br>
    <div style="font-size: 25px; display: flex; flex-direction: row"> <div style="width: 100px; color: green; border-radius: 12px">GET</div><div style="border-left: 1px solid black; padding-left: 12px;"></div>      <div style="width:250px">Get Product Details:</div>  <a style="font-size: 20px; align-self: center; padding-left: 12px;">${myIpAddress}/trainings/products/$id</a></div>
    <br>
    <div style="font-size: 25px; display: flex; flex-direction: row"> <div style="width: 100px; color: orange; border-radius: 12px">POST</div><div style="border-left: 1px solid black; padding-left: 12px;"></div>    <div style="width:250px">Create Product:</div>     <a style="font-size: 20px; align-self: center; padding-left: 12px;"   >${myIpAddress}/trainings/product &nbsp;&nbsp; req: {name: string, price: number, details: Object}</a></div>
    <br>
    <div style="font-size: 25px; display: flex; flex-direction: row"> <div style="width: 100px; color: blueviolet; border-radius: 12px">PUT</div><div style="border-left: 1px solid black; padding-left: 12px;"></div> <div style="width:250px">Update Product:</div>  <a style="font-size: 20px; align-self: center; padding-left: 12px;">          ${myIpAddress}/trainings/products/$id &nbsp;&nbsp; req: {name: string, price: number, details: Object}</a></div>
    <br>
    <div style="font-size: 25px; display: flex; flex-direction: row"> <div style="width: 100px; color: red; border-radius: 12px">DELETE</div><div style="border-left: 1px solid black; padding-left: 12px;"></div>     <div style="width:250px">Delete Product:</div>     <a style="font-size: 20px; align-self: center; padding-left: 12px;">${myIpAddress}/trainings/products/$id</a></div>
    
    <br>
    <br>
    <br>
    <div style="width: 100%; display: flex; flex-direction: row; justify-content: center; align-items: center">
        <img src="https://media.giphy.com/media/IB9foBA4PVkKA/giphy.gif"/>
    </div>
</div>
`);
};

exports.hello = function (req, res) {
  res.send({status: 'OK', timestamp: new Date()});
};

exports.request_coffe_break = function (req, res, next) {
  CoffeBreak.find({ip_address: req.connection.remoteAddress}).then(la => {
    if (la.length === 0) {
      var coffeBreak = new CoffeBreak({ip_address: req.connection.remoteAddress});
      coffeBreak.save(function (err, response) {
        CoffeBreak.find({}).exec().then(r => {
          timesRequested = r.length;
          if (timesRequested >= requestsTillCoffeeBreak) {
            play.sound('./assets/bell.wav');
            opn('https://coub.com/view/116ozw?autostart=true');
            CoffeBreak.deleteMany({}, function () {
              console.log('deleted');
            });
          }
          res.send({
            message: 'Coffee break requested',
            requests_remaining: requestsTillCoffeeBreak - r.length
          });
        });

        if (err) {
          return next(err);
        }
      })
    } else {
      CoffeBreak.find({}).exec().then(r => {
        res.status(418).send({
          message: `You already requested a coffee break. Patience, young one...`,
          requests_remaining: requestsTillCoffeeBreak - r.length
        })
      });
    }
  });
};

exports.get_coffe_break_count = function (req, res, next) {
  counter++;
  console.log('counter:', counter);
  CoffeBreak.find({}).exec().then(r => {
    res.send({count: requestsTillCoffeeBreak - r.length});
  });

};

exports.product_create = function (req, res, next) {
  var product = new Product(
    {
      image: req.body.image,
      carMake: req.body.carMake,
      carYear: req.body.carYear,
      carPrice: req.body.carPrice,
      estimateRepair: req.body.estimateRepair,
      repairPrice: undefined,
      owner_id: undefined
    }
  );

  product.save(function (err) {
    if (err) {
      console.log('error:', err);
      return next(err);
    }
    res.send({message: 'Product Created successfully!'})
  })
};

exports.product_list = function (req, res, next) {
  var response;
  var pageNo = parseInt(req.query.page);
  var size = parseInt(req.query.size);
  var query = {};
  if (pageNo && size) {
    if (pageNo < 0 || pageNo === 0) {
      response = {"error": true, "message": "invalid page number, should start with 1"};
      return res.json(response)
    }
    query.skip = size * (pageNo - 1);
    query.limit = size;
    query.sort = {created_time: -1}
    // Find some documents
    console.log('will find some');
    Product.find({owner_id: undefined}, {}, query, function (err, data) {
      // Mongo command to fetch all data from collection.
      if (err) {
        response = {"error": true, "message": "Error fetching data"};
      } else {
        response = data;
        const returnList = response.map(p => {
          return {
            image: req.body.image,
            carMake: req.body.carMake,
            carYear: req.body.carYear,
            carPrice: req.body.carPrice,
            estimateRepair: req.body.estimateRepair,
            repairPrice: undefined,
          }
        });
        res.send(returnList);
      }
    });
  } else {
    query.sort = {created_time: -1}
    Product.find({owner_id: undefined}, {}, query).then(products => {
      const returnList = products.map(p => {
        console.log('p', p);
        // image: req.body.image,
        //   carMake: req.body.carMake,
        //   carYear: req.body.carYear,
        //   carPrice: req.body.carPrice,
        //   estimateRepair: req.body.estimateRepair,
        //   repairPrice: undefined
        return {
          id: p._id,
          image: p.image,
          carMake: p.carMake,
          carYear: p.carYear,
          carPrice: p.carPrice,
          estimateRepair: p.estimateRepair,
          created_time: p.created_time,
          owner_id: p.owner_id
        }
      });
      res.send(returnList);
    })
  }

};

exports.product_list_by_ip = function (req, res, next) {
  var response;
  var pageNo = parseInt(req.query.page);
  var size = parseInt(req.query.size);
  var query = {};
  // if (pageNo && size) {
    if (pageNo < 0 || pageNo === 0) {
      response = {"error": true, "message": "invalid page number, should start with 1"};
      return res.json(response)
    }
    query.skip = size * (pageNo - 1);
    query.limit = size;
    query.sort = {created_time: -1}
    // Find some documents
    Product.find({owner_id: req.params.owner_id}, {}, query, function (err, data) {
      // Mongo command to fetch all data from collection.
      if (err) {
        console.log('err', err);
        response = {"error": true, "message": "Error fetching data"};
      } else {
        console.log('wow found', data);
        response = data;
        const returnList = response.map(p => {
          console.log('p:', p);
          return {
            id: p.id,
            image: p.image,
            carMake: p.carMake,
            carYear: p.carYear,
            carPrice: p.carPrice,
            estimateRepair: p.estimateRepair,
            repairPrice: p.repairPrice
          }
        });
        res.send(returnList);
      }
    });
  /*} else {
    query.sort = {created_time: -1}
    Product.find({}, {}, query).then(products => {
      const returnList = products.map(p => {
        return {
          image: req.body.image,
          carMake: req.body.carMake,
          carYear: req.body.carYear,
          carPrice: req.body.carPrice,
          estimateRepair: req.body.estimateRepair,
          repairPrice: undefined
        }
      });
      res.send(returnList);
    })
  }*/

};

exports.buy_car = function (req, res, next) {
  let returnProduct = {};
  Product.findById(req.params.id, function (err, p) {
    if (err) return next(err);
    if (!p) {
      res.status(404).send({message: "It's gone, bro"});
      return;
    }
    console.log('found product:', p);
    // returnProduct = {id: p._id, name: p.name, price: p.price, details: p.details};
    // console.log('returnProduct', returnProduct);

    var purchasedCar;
    Product.findById(req.params.id, function (err, p) {
      if (!p) {
        res.status(404).send({message: "No such car in dealership"});
      } else {
        console.log('will purchase car for owner:', req.body);
        purchasedCar = new Product(
          {
            image: p.image,
            carMake: p.carMake,
            carYear: p.carYear,
            carPrice: p.carPrice,
            estimateRepair: p.estimateRepair,
            repairPrice: p.estimateRepair ? p.estimateRepair * (1 + 0.2 * (Math.random() * 2 - 1)) : 0,
            // ip_address: req.connection.remoteAddress,
            owner_id: req.body.ownerId
          }
        );
        Product.findByIdAndRemove(req.params.id, function (err) {
          if (err) return send(err);
          // console.log('removed id:', req.params.id, ' successfully');
        });

        purchasedCar.save(function (err) {
          if (err) {
            console.log('error:', err);
            return next(err);
          }

          res.send({message: 'You successfully purchased ' + purchasedCar.carMake})
        });
      }
    });

  });
};

exports.product_details = function (req, res, next) {
  let returnProduct = {};
  Product.findById(req.params.id, function (err, p) {
    if (err) return next(err);
    if (!p) {
      res.status(404).send({message: "It's gone, bro"});
      return;
    }
    console.log('found product:', p);
    returnProduct = {id: p._id, name: p.name, price: p.price, details: p.details};
    console.log('returnProduct', returnProduct);
    res.send(returnProduct);
  });
};

exports.product_update = function (req, res, next) {
  Product.findByIdAndUpdate(req.params.id, {$set: {...req.body, created_time: new Date()}}, function (err, product) {
    if (err) {
      console.log('error:', err);
      return next(err);
    }

    res.send({message: 'Product Udpated successfully!', object: {...req.body, editing: false}});
  });
};

exports.product_delete = function (req, res, next) {
  Product.findById(req.params.id, function (err, p) {
    if (!p) {
      res.status(404).send({message: "Issiplauk galva"});
    } else {
      Product.findByIdAndRemove(req.params.id, function (err) {
        if (err) return send(err);
        res.send({message: 'Product Deleted successfully!'});
      })
    }
  });
};

exports.product_delete_by_owner = function (req, res, next) {
  Product.findById(req.params.id, function (err, p) {
    if (!p) {
      res.status(404).send({message: "No such car"});
    } else {
      if (p.owner_id !== req.body.ownerId) {
        res.status(403).send({message: "You don't own this car"});
      }
      Product.findByIdAndRemove(req.params.id, function (err) {
        if (err) return send(err);
        res.send({message: 'Car sold successfully. Nice!'});
      })
    }
  });
};

exports.delete_everything = function (req, res, next) {
  Product.deleteMany({}, err => console.log('products deleted'));
  CoffeBreak.deleteMany({}, err => console.log('coffee requests deleted'));
  res.send({message: 'Everything is gone now...'});
};
