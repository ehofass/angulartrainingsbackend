var express = require('express');
var bodyParser = require('body-parser');

var product = require('./routes/product'); // Imports routes for the products

var axelra = require('./routes/axelra');

var alexraController = require('./controllers/axelra');

const WebSocket = require('ws');
var http = require('http');


var app = express();


// Set up mongoose connection
var mongoose = require('mongoose');

var dev_db_url = 'mongodb+srv://trainings:trainings@cluster0-vzf3t.mongodb.net/test?retryWrites=true&w=majority';
var mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// app.use('/trainings', product);

app.use('/axelra', axelra);


const server = http.createServer(app);
const wss = new WebSocket.Server({server});
alexraController.set_wss(wss);

server.listen(process.env.PORT || 3001);
